import { cposTemplatePage } from './app.po';

describe('cpos App', function() {
  let page: cposTemplatePage;

  beforeEach(() => {
    page = new cposTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
