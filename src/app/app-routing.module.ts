import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { AppRouteGuard } from '@shared/auth/auth-route-guard';

const routes: Routes = [ 
  {
    path: 'login',
    loadChildren: './pages/authentication/login/login.module#LoginModule',
  },
  {
    path: 'register',
    loadChildren: './pages/authentication/register/register.module#RegisterModule',
  },
  {
    path: 'forgot-password',
    loadChildren: './pages/authentication/forgot-password/forgot-password.module#ForgotPasswordModule',
  },
  {
    path: 'coming-soon',
    loadChildren: './pages/coming-soon/coming-soon.module#ComingSoonModule',
  },
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        loadChildren: './pages/dashboard/dashboard.module#DashboardModule',
        pathMatch: 'full',
      },
      {
        path: 'roles',
        loadChildren: './pages/roles/roles.module#RolesModule',
        canActivate: [AppRouteGuard],
      },
      {
        path: 'apps/inbox',
        loadChildren: './pages/apps/inbox/inbox.module#InboxModule',
        canActivate: [AppRouteGuard],
      },
      {
        path: 'apps/calendar',
        loadChildren: './pages/apps/calendar/calendar.module#CalendarAppModule',
        canActivate: [AppRouteGuard],
      },
      {
        path: 'apps/chat',
        loadChildren: './pages/apps/chat/chat.module#ChatModule',
        canActivate: [AppRouteGuard],
      },
      {
        path: 'components',
        loadChildren: './pages/components/components.module#ComponentsModule',
        canActivate: [AppRouteGuard],
      },
      {
        path: 'forms/form-elements',
        loadChildren: './pages/forms/form-elements/form-elements.module#FormElementsModule',
        canActivate: [AppRouteGuard],
      },
      {
        path: 'forms/form-wizard',
        loadChildren: './pages/forms/form-wizard/form-wizard.module#FormWizardModule',
        canActivate: [AppRouteGuard],
      },
      {
        path: 'icons',
        loadChildren: './pages/icons/icons.module#IconsModule',
        canActivate: [AppRouteGuard],
      },
      {
        path: 'page-layouts',
        loadChildren: './pages/page-layouts/page-layouts.module#PageLayoutsModule',
        canActivate: [AppRouteGuard],
      },
      {
        path: 'maps/google-maps',
        loadChildren: './pages/maps/google-maps/google-maps.module#GoogleMapsModule',
        canActivate: [AppRouteGuard],
      },
      {
        path: 'tables/all-in-one-table',
        loadChildren: './pages/tables/all-in-one-table/all-in-one-table.module#AllInOneTableModule',
        canActivate: [AppRouteGuard],
      },
      {
        path: 'drag-and-drop',
        loadChildren: './pages/drag-and-drop/drag-and-drop.module#DragAndDropModule',
        canActivate: [AppRouteGuard],
      },
      {
        path: 'editor',
        loadChildren: './pages/editor/editor.module#EditorModule',
        canActivate: [AppRouteGuard],
      },
      {
        path: 'blank',
        loadChildren: './pages/blank/blank.module#BlankModule',
        canActivate: [AppRouteGuard],
      },
      {
        path: 'level1/level2/level3/level4/level5',
        loadChildren: './pages/level5/level5.module#Level5Module',
        canActivate: [AppRouteGuard],
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled',
    preloadingStrategy: PreloadAllModules,
    scrollPositionRestoration: 'enabled',
    anchorScrolling: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
