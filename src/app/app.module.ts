import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import 'hammerjs'; // Needed for Touch functionality of Material Components
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutModule } from './layout/layout.module';
import { AgmCoreModule } from '@agm/core';
import { environment } from '../environments/environment';
import { PendingInterceptorModule } from '../@fury/shared/loading-indicator/pending-interceptor.module';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions } from '@angular/material/form-field';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS, MatSnackBarConfig } from '@angular/material/snack-bar';

import { HAMMER_GESTURE_CONFIG, BrowserModule } from '@angular/platform-browser';
import {  NoopAnimationsModule } from '@angular/platform-browser/animations';
import {  Injector, APP_INITIALIZER, LOCALE_ID } from '@angular/core';
import { PlatformLocation, registerLocaleData, CommonModule } from '@angular/common';

import { AbpModule } from '@abp/abp.module';
import { AbpHttpInterceptor } from '@abp/abpHttpInterceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { SharedModule } from '@shared/shared.module';
import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';

import { AppConsts } from '@shared/AppConsts';
import { AppSessionService } from '@shared/session/app-session.service';
import { API_BASE_URL } from '@shared/service-proxies/service-proxies';

import { AppPreBootstrap } from '../AppPreBootstrap';
import { ModalModule } from 'ngx-bootstrap';

import { GestureConfig } from '@angular/material';

import * as _ from 'lodash';
import { LoadingIndicatorModule } from '@fury/shared/loading-indicator/loading-indicator.module';
import { FurySharedModule } from '@fury/fury-shared.module';

// export function appInitializerFactory(injector: Injector,
//   platformLocation: PlatformLocation) {
//   return () => {

//     abp.ui.setBusy();
//     return new Promise<boolean>((resolve, reject) => {
//       AppConsts.appBaseHref = getBaseHref(platformLocation);
//       const appBaseUrl = getDocumentOrigin() + AppConsts.appBaseHref;

//       AppPreBootstrap.run(appBaseUrl, () => {
//         abp.event.trigger('abp.dynamicScriptsInitialized');
//         const appSessionService: AppSessionService = injector.get(AppSessionService);
//         appSessionService.init().then(
//           (result) => {
//             abp.ui.clearBusy();

//             if (shouldLoadLocale()) {
//               const angularLocale = convertAbpLocaleToAngularLocale(abp.localization.currentLanguage.name);
//               import(`@angular/common/locales/${angularLocale}.js`)
//                 .then(module => {
//                   registerLocaleData(module.default);
//                   resolve(result);
//                 }, reject);
//             } else {
//               resolve(result);
//             }
//           },
//           (err) => {
//             abp.ui.clearBusy();
//             reject(err);
//           }
//         );
//       });
//     });
//   };
// }

// export function convertAbpLocaleToAngularLocale(locale: string): string {
//   if (!AppConsts.localeMappings) {
//     return locale;
//   }

//   const localeMapings = _.filter(AppConsts.localeMappings, { from: locale });
//   if (localeMapings && localeMapings.length) {
//     return localeMapings[0]['to'];
//   }

//   return locale;
// }

// export function shouldLoadLocale(): boolean {
//   return abp.localization.currentLanguage.name && abp.localization.currentLanguage.name !== 'en-US';
// }

// export function getRemoteServiceBaseUrl(): string {
//   return AppConsts.remoteServiceBaseUrl;
// }

// export function getCurrentLanguage(): string {
//   if (abp.localization.currentLanguage.name) {
//     return abp.localization.currentLanguage.name;
//   }

//   // todo: Waiting for https://github.com/angular/angular/issues/31465 to be fixed.
//   return 'en';
// }



@NgModule({
  imports: [
    // // Angular Core Module // Don't remove!
    // BrowserModule,
    // BrowserAnimationsModule,
    // HttpClientModule,
    
    // SharedModule.forRoot(),
    // ModalModule.forRoot(),
    // AbpModule,
    // ServiceProxyModule,

    LoadingIndicatorModule,
    FurySharedModule,
    
    // Fury Core Modules
    AppRoutingModule,

    // Layout Module (Sidenav, Toolbar, Quickpanel, Content)
    LayoutModule,

    // Google Maps Module
    AgmCoreModule.forRoot({
      //apiKey: environment.googleMapsApiKey
    }),

    // Displays Loading Bar when a Route Request or HTTP Request is pending
    PendingInterceptorModule,

    // Register a Service Worker (optional)
    // ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  declarations: [
    AppComponent
  ],
  providers: [
    // { provide: HTTP_INTERCEPTORS, useClass: AbpHttpInterceptor, multi: true },
    // { provide: API_BASE_URL, useFactory: getRemoteServiceBaseUrl },
    // {
    //   provide: APP_INITIALIZER,
    //   useFactory: appInitializerFactory,
    //   deps: [Injector, PlatformLocation],
    //   multi: true
    // },
    // {
    //   provide: LOCALE_ID,
    //   useFactory: getCurrentLanguage
    // },
    // { provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig },
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'fill'
      } as MatFormFieldDefaultOptions
    },
    {
      provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
      useValue: {
        duration: 5000,
        horizontalPosition: 'end',
        verticalPosition: 'bottom'
      } as MatSnackBarConfig
    }
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
}

// export function getBaseHref(platformLocation: PlatformLocation): string {
//   const baseUrl = platformLocation.getBaseHrefFromDOM();
//   if (baseUrl) {
//     return baseUrl;
//   }

//   return '/';
// }

// function getDocumentOrigin() {
//   if (!document.location.origin) {
//     const port = document.location.port ? ':' + document.location.port : '';
//     return document.location.protocol + '//' + document.location.hostname + port;
//   }

//   return document.location.origin;
// }
