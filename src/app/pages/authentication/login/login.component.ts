import { ChangeDetectorRef, Component, OnInit, Injector } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { fadeInUpAnimation } from '../../../../@fury/animations/fade-in-up.animation';
import { LoginService } from 'account/login/login.service';
import { AbpSessionService } from 'abp-ng2-module/dist/src/session/abp-session.service';
import { AppComponentBase } from '@shared/app-component-base';

@Component({
  selector: 'fury-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [fadeInUpAnimation]
})
export class LoginComponent extends AppComponentBase implements OnInit {


  submitting = false;
  form: FormGroup;

  inputType = 'password';
  visible = false;

  constructor(
    injector: Injector,
    public loginService: LoginService,
    private _sessionService: AbpSessionService,
    private router: Router,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private snackbar: MatSnackBar
  ) {
    super(injector);
  }

  get multiTenancySideIsTeanant(): boolean {
    return this._sessionService.tenantId > 0;
  }

  get isSelfRegistrationAllowed(): boolean {
    if (!this._sessionService.tenantId) {
      return false;
    }

    return true;
  }

  // login(): void {
  //   this.submitting = true;
  //   this.loginService.authenticate(() => (this.submitting = false));
  // }


  ngOnInit() {
    this.form = this.fb.group({
      userNameOrEmailAddress: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  send() {
    this.submitting = true;
    this.loginService.authenticate(() => (this.submitting = false));

    // this.router.navigate(['/']);
    // this.snackbar.open('Lucky you! Looks like you didn\'t need a password or email address! For a real application we provide validators to prevent this. ;)', 'LOL THANKS', {
    //   duration: 10000
    // });
  }

  toggleVisibility() {
    if (this.visible) {
      this.inputType = 'password';
      this.visible = false;
      this.cd.markForCheck();
    } else {
      this.inputType = 'text';
      this.visible = true;
      this.cd.markForCheck();
    }
  }
}
