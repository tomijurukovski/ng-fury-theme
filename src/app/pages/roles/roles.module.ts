import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BreadcrumbsModule } from '../../../@fury/shared/breadcrumbs/breadcrumbs.module';
import { ListModule } from '../../../@fury/shared/list/list.module';
import { MaterialModule } from '../../../@fury/shared/material-components.module';
import { RolesRoutingModule } from './roles-routing.module';
import { RolesComponent } from './roles.component';
import { RoleCreateUpdateModule } from './role-create-update/role-create-update.module';
import { FurySharedModule } from '../../../@fury/fury-shared.module';
import { RoleServiceProxy } from '@shared/service-proxies/service-proxies';

@NgModule({
  imports: [
    CommonModule,
    RolesRoutingModule,
    FormsModule,
    MaterialModule,
    FurySharedModule,

    // Core
    ListModule,
    RoleCreateUpdateModule,
    BreadcrumbsModule
  ],
  providers: [
    RoleServiceProxy
  ],
  declarations: [RolesComponent],
  exports: [RolesComponent]
})
export class RolesModule {
}
