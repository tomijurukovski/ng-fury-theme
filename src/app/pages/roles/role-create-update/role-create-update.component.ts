import { Component, Inject, OnInit, Injector, Optional } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { RoleDto, RoleServiceProxy, ListResultDtoOfPermissionDto, PermissionDto, CreateRoleDto, GetRoleForEditOutput } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';
import { MatCheckboxChange } from '@angular/material';
import * as _ from 'lodash';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'fury-role-create-update',
  templateUrl: './role-create-update.component.html',
  styleUrls: ['./role-create-update.component.scss']
})
export class RoleCreateUpdateComponent extends AppComponentBase
 implements OnInit {

  static id = 0;

  saving = false;
  role: RoleDto = new RoleDto();
  permissions: PermissionDto[] = [];
  grantedPermissionNames: string[] = [];
  checkedPermissionsMap: { [key: string]: boolean } = {};
  defaultPermissionCheckedStatus = true;

  form: FormGroup;
  mode: 'create' | 'update' = 'create';

  constructor(
    injector: Injector,
    private _roleService: RoleServiceProxy,
    private dialogRef: MatDialogRef<RoleCreateUpdateComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) private _id: number,
    private fb: FormBuilder
  ) {
    super(injector);
  }

  ngOnInit() {
    if (this._id) {
      this.mode = 'update';
    } else {
      this.mode = 'create';
    }

    if (this.mode == 'update') {
      this._roleService
        .getRoleForEdit(this._id)
        .subscribe((result: GetRoleForEditOutput) => {
          this.role.init(result.role);
          _.map(result.permissions, item => {
            const permission = new PermissionDto();
            permission.init(item);
            this.permissions.push(permission);
          });
          this.grantedPermissionNames = result.grantedPermissionNames;
          this.setInitialPermissionsStatus();
        });
    } else {
      this._roleService
        .getAllPermissions()
        .subscribe((result: ListResultDtoOfPermissionDto) => {
          this.permissions = result.items;
          this.setInitialPermissionsStatus();
        });
    }

    this.form = this.fb.group({
      name: '',
      displayName: '',
      description: ''
    });
  }

  setInitialPermissionsStatus(): void {
    _.map(this.permissions, item => {
      this.checkedPermissionsMap[item.name] = this.isPermissionChecked(
        item.name
      );
    });
  }

  isPermissionChecked(permissionName: string): boolean {
    if (this.isCreateMode())
      // just return default permission checked status
      // it's better to use a setting
      return this.defaultPermissionCheckedStatus;
    else
      return _.includes(this.grantedPermissionNames, permissionName);
  }

  onPermissionChange(permission: PermissionDto, $event: MatCheckboxChange) {
    this.checkedPermissionsMap[permission.name] = $event.checked;
  }

  getCheckedPermissions(): string[] {
    const permissions: string[] = [];
    _.forEach(this.checkedPermissionsMap, function (value, key) {
      if (value) {
        permissions.push(key);
      }
    });
    return permissions;
  }


  save() {
    this.saving = true;

    // this.role = this.form.value;
    this.role.grantedPermissions = this.getCheckedPermissions();

    if (this.mode === 'create') {
      this.createRole();
    } else if (this.mode === 'update') {
      this.updateRole();
    }
  }

  createRole() {
    const role_ = new CreateRoleDto();
    role_.init(this.role);

    debugger;

    this._roleService
      .create(role_)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });
  }

  updateRole() {
    // const role = this.form.value;
    // role.id = this.defaults.id;

    this._roleService
      .update(this.role)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  isDisabled(){
    if (this.isCreateMode()) 
      return this.role;
    else
      return null;
  }

  close(result: any): void {
    this.dialogRef.close(result);
  }
}
